
import utfpr.ct.dainf.if62c.pratica.Circulo;
import utfpr.ct.dainf.if62c.pratica.Elipse;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author João Paulo
 */
public class Pratica41 {
    
    public static void main(String[] args) {
        Elipse e1 = new Elipse(2.5, 3.5);
        Elipse e2 = new Elipse(2, 5);
        Circulo c1 = new Circulo(5);
        Circulo c2 = new Circulo(1.5);

        System.out.println("E1: P = "+e1.getPerimetro()+" | A = "+e1.getArea());
        System.out.println("E2: P = "+e2.getPerimetro()+" | A = "+e2.getArea()); 
        System.out.println("C1: P = "+c1.getPerimetro()+" | A = "+c1.getArea());
        System.out.println("C2: P = "+c2.getPerimetro()+" | A = "+c2.getArea());
    }        
}
