/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author João Paulo
 */
public class Elipse {
    
    //Semi-eixos
    protected double r;
    protected double s;
    
    //Construtor Padrão
    public Elipse() {}
    
    //Construtor que inicializa os valores dos semi eixos
    public Elipse(double r, double s) {
        this.r = r;
        this.s = s;
    }
    
    public double getArea()
    {
        return Math.PI*this.r*this.s;
    }
    
    public double getPerimetro()
    {
        return Math.PI*(3*(r+s)-Math.sqrt((3*r+s)*(r+3*s)));
    }
    
}
